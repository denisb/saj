# SAJ

Simple (public) Arm JupyterLab (Démo, à utiliser avec Binder)

En statique, ou dynamique (avec binder ci-dessous) :
* un modèle de fichier pour démarrer : bonjour.ipynb

Pour la version dynamique (la plus intéressante), si vous êtes déjà adepte de Jupyter, vous pouvez essayer de lancer ces notebook sur votre machine (attention, il y a des prérequis, regarder apt.txt) ou choisissez votre serveur Binder :
* Binder sur gricad (version conseillée, mais seulement pour ceux qui ont un vpn uga) : [![Binder](https://binderhub.univ-grenoble-alpes.fr/badge_logo.svg)](https://binderhub.univ-grenoble-alpes.fr/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fsaj/HEAD)
* myBynder (pour les autres, ceux qui n'ont pas de vpn uga): [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgricad-gitlab.univ-grenoble-alpes.fr%2Fdenisb%2Fsaj/HEAD)
